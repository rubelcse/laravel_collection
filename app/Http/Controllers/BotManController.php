<?php

namespace App\Http\Controllers;

use BotMan\BotMan\BotMan;
use Illuminate\Http\Request;
use App\Conversations\ExampleConversation;
use BotMan\Drivers\Facebook\Extensions\ButtonTemplate;
use BotMan\Drivers\Facebook\Extensions\ElementButton;
use BotMan\Drivers\Facebook\Extensions\GenericTemplate;
use BotMan\Drivers\Facebook\Extensions\Element;
use BotMan\Drivers\Facebook\Extensions\ReceiptTemplate;
use BotMan\Drivers\Facebook\Extensions\ReceiptSummary;
use BotMan\Drivers\Facebook\Extensions\ReceiptAdjustment;
use BotMan\Drivers\Facebook\Extensions\ReceiptElement;
use BotMan\Drivers\Facebook\Extensions\ReceiptAddress;
use BotMan\Drivers\Facebook\Extensions\ListTemplate;

class BotManController extends Controller
{
    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        $link = "http://www.radiobangla.co/api/highlighted-news";
        $data = json_decode(file_get_contents($link),true);
        $botman = app('botman');

        // our first BotMan command
        $botman->hears('hello', function ($bot) {
            $bot->reply("Hello, I'm Hello World bot!");
        });
        $botman->hears('hi', function (BotMan $bot) {
            $bot->reply('Hello buddy.');
        });

        $botman->hears('how are you', function (BotMan $bot) {
            $bot->reply("Fine");
        });

        $botman->hears('help', function (BotMan $bot) {
            $bot->typesAndWaits(2);
            $bot->reply("Please contact me...019-711-SAjNE(72563)!");
        });

        $botman->hears('call me {name}', function (BotMan $bot,$name) {
            $bot->reply("Yeah...Please wait ".$name);
        });

        $botman->fallback(function($bot) {
            //$bot->reply('Sorry, I did not understand these commands. Here is a list of commands I understand: ...');
            $bot->reply(ButtonTemplate::create('Do you want to know more about sajne?')
                ->addButton(ElementButton::create('Tell me more')->type('postback')->payload('tellmemore'))
                ->addButton(ElementButton::create('Show Details')->url('http://demo.sajne.com/sajne-help'))
            );
        });

        /*$botman->hears('sajne', function (BotMan $bot) {
            // Build message object
            $message = Message::create('This is my text')
                ->image('http://sajne.com/media/wysiwyg/adds/Banner_Grand_Opening.png');

            // Reply message object
            $bot->reply($message);
        });*/

        $botman->hears('sajne info', function (BotMan $bot) {
            // Build message object
            $bot->reply(ButtonTemplate::create('Do you want to know more about sajne?')
                ->addButton(ElementButton::create('Tell me more')->type('postback')->payload('tellmemore'))
                ->addButton(ElementButton::create('Show Details')->url('http://demo.sajne.com/sajne-help'))
            );
        });

//carasoll view
        $botman->hears('latest product', function (BotMan $bot) {
            // Build message object
            $bot->reply(GenericTemplate::create()
                ->addImageAspectRatio(GenericTemplate::RATIO_SQUARE)
                ->addElements([
                    Element::create('sajne Product')
                        ->subtitle('product of sajne')
                        ->image('http://sajne.com/skin/frontend/ma_mozar/ma_mozar2/images/sajne_logo.png')
                        ->addButton(ElementButton::create('visit')->url('http://sajne.com/'))
                        ->addButton(ElementButton::create('More Product')->url('http://sajne.com/')
                            ->payload('tellmemore')->type('postback')),
                    Element::create('Female Brand')
                        ->subtitle('Sajne only offers 100% new and unused products. The only exception is for refurbished laptops for which we ensure the best quality grade and full warranty from the vendor. ')
                        ->image('http://sajne.com/media/catalog/product/cache/2/rotator_image/540x728/9df78eab33525d08d6e5fb8d27136e95/n/e/netiya_1202d-2.jpg')
                        ->addButton(ElementButton::create('visit')
                            ->url('http://sajne.com/kurti-chinese-linen-with-embroidery-work.html')
                        ),
                    Element::create('Male Brand')
                        ->subtitle('Best sell sajne')
                        ->image('http://sajne.com/media/wysiwyg/adds/sajne_add_banner_316-219_05.jpg')
                        ->addButton(ElementButton::create('visit')
                            ->url('http://sajne.com/men-s-polo-shirt.html')
                        )
                ])
            );
        });

//$botman->hears('test', 'MyBotCommands@handleFoo');

//list of view of a product
        $botman->hears('latest product list', function (BotMan $bot) {
            $bot->reply(ListTemplate::create()
                ->useCompactView()
                ->addGlobalButton(ElementButton::create('view more')->url('http://sajne.com/kurti-chinese-linen-with-embroidery-work.html'))
                ->addElement(
                    Element::create('Female Product')
                        ->subtitle('All about Female Product')
                        ->image('http://sajne.com/media/catalog/product/cache/2/rotator_image/540x728/9df78eab33525d08d6e5fb8d27136e95/n/e/netiya_1202d-2.jpg')
                        ->addButton(ElementButton::create('visit')
                            ->payload('tellmemore')->type('postback'))
                )
                ->addElement(
                    Element::create('Male Product')
                        ->subtitle('This is the best way to Buy with Sajne')
                        ->image('http://sajne.com/media/wysiwyg/adds/sajne_add_banner_316-219_05.jpg')
                        ->addButton(ElementButton::create('visit')
                            ->url('http://sajne.com/men-s-polo-shirt.html')
                        )
                )
            );
        });

//product sell
        $botman->hears('product sell', function (BotMan $bot) {
            $bot->reply(
                ReceiptTemplate::create()
                    ->recipientName('Rubel Ahmed')
                    ->merchantName('Sajne')
                    ->orderNumber('342343434343')
                    ->timestamp('1428444852')
                    ->orderUrl('http://sajne.com/')
                    ->currency('USD')
                    ->paymentMethod('VISA')
                    ->addElement(ReceiptElement::create('T-Shirt Small')->price(15.99)->image('http://sajne.com/media/catalog/product/cache/2/image/450x600/9df78eab33525d08d6e5fb8d27136e95/u/n/untitledp0i0-1.jpg'))
                    ->addElement(ReceiptElement::create('T-Shirt Large')->price(2.99)->image('http://sajne.com/media/catalog/product/cache/2/rotator_image/540x728/9df78eab33525d08d6e5fb8d27136e95/u/n/untitled2-3.jpg'))
                    ->addAddress(ReceiptAddress::create()
                        ->street1('Mirpur 10')
                        ->city('Dhaka City')
                        ->postalCode(120000)
                        ->state('shenpara')
                        ->country('Bangladesh')
                    )
                    ->addSummary(ReceiptSummary::create()
                        ->subtotal(18.98)
                        ->shippingCost(10 )
                        ->totalTax(15)
                        ->totalCost(23.98)
                    )
                    ->addAdjustment(ReceiptAdjustment::create('Sajne Bonus')->amount(5))
            );
        });


//latest news
        $botman->hears('latest news', function (BotMan $bot) {
            // Build message object
            $bot->typesAndWaits(2);
            $link = "http://radiobangla.us/api/highlighted-news";
            $data = json_decode(file_get_contents($link),true);
            foreach ($data[0]['relations'] as $d) {
                $latestNews[]=Element::create($d['title'])
                    ->image('http://radiobangla.us/uploads/news/extra-small/' . $d['main_image'][0]['image'])
                    ->addButton(ElementButton::create('read more')
                        ->url('http://radiobangla.us/news/' . $d['id'])
                    );
            }
            $bot->reply(GenericTemplate::create()
                ->addImageAspectRatio(GenericTemplate::RATIO_SQUARE)
                ->addElements($latestNews)
            );


        });

        $botman->listen();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tinker()
    {
        return view('tinker');
    }

    /**
     * Loaded through routes/botman.php
     * @param  BotMan $bot
     */
    public function startConversation(BotMan $bot)
    {
        $bot->startConversation(new ExampleConversation());
    }
}
