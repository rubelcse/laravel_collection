<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function colletTest()
    {
        $users = User::all();
        //dd($users);
       $chunks = $users->chunk(2)->toArray();
       //dd($chunks);
        return view('collect.collect',compact('chunks'));
    }

}
