<?php
Route::get('/', function () {
    $collections = collect([
       ['name'=>'banana','category'=>'fruit','price'=>55.25,'qualifise_free'=>true,'status'=>'active'] ,
       ['name'=>'sweet','category'=>'food','price'=>200.00] ,
       ['name'=>'shirt','category'=>'wear','price'=>200.50,'status'=>'active'] ,
       ['name'=>'ghe','category'=>'mosla','price'=>20.50,'qualifise_free'=>true,'status'=>'active'] ,
       ['name'=>null,'category'=>'fashion','price'=>20.50,'qualifise_free'=>true] ,
    ]);
    $name = $collections->pluck('name');
    $upperCaseName = $name->map(function ($name){
        return strtoupper($name);
    })->reject(function ($name){
       return empty($name);
    });
   $price = $collections->pluck('price');
   $multiplyPrice = $price->map(function ($item){
      return $item + 2;
   });
   $activeuser = $collections->map(function ($collections){
      return $collections['name'];
   });

    $free= $collections->where('qualifise_free',true);
    $freeProductTotal = $free->sum('price');
    $totalPrice = $collections->sum('price');
    $netPrice = $totalPrice - $freeProductTotal;
    $collectionCategorys = $collections->groupBy('category');
    return view('home',compact('netPrice'))
        ->with('totalPrice',$totalPrice)
        ->with('freeProductTotal',$freeProductTotal)
        ->with('groupByCategorys',$collectionCategorys)
        ;
});

Route::get('/chunk-out','collectionController@chunkOut');

Route::match(['get', 'post'], '/botman', 'BotManController@handle');
Route::get('/botman/tinker', 'BotManController@tinker');

Route::get('/collect','HomeController@colletTest');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('api/users/{user}', function ($id) {
    return $id;
});

Route::prefix('admin')->middleware('auth')->namespace('Admin')->group(function (){
    Route::resource('user','UserController',['only'=>'create','index']);
});








