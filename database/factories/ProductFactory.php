<?php
$factory->define(App\Product::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'type' => $faker->name,
        'quantity' => $faker->unique()->numberBetween(0,20),
        'price' => $faker->unique()->numberBetween(0,30),
    ];
});