@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <table class="table table-striped table-hover ">
                        <thead>
                        <tr>
                            <th>SL No</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1 ?>
                            @foreach($groupByCategorys as $categorys)
                               @foreach($categorys as $category)
                                    <tr class="info">
                                        <td>{{$i++}}</td>
                                        <td>{{$category['name']}}</td>
                                        <td>{{$category['category']}}</td>
                                        <td>{{$category['price']}}</td>
                                    </tr>
                               @endforeach
                            @endforeach
                        </tbody>

                    </table>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <span class="badge">&dollar;{{$totalPrice}}</span>
                            Total Price
                        </li>
                        <li class="list-group-item">
                            <span class="badge">&dollar;{{$freeProductTotal}}</span>
                            Total Discount
                        </li>
                        <li class="list-group-item">
                            <span class="badge">&dollar;{{$netPrice}}</span>
                            Net Price
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
