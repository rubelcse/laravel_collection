@extends('layouts.app')

@section('content')
    @foreach ($products->chunk(3) as $chunk)
        <div class="row">
            @foreach ($chunk as $product)
                <div class="col-xs-4 ">{{ $product->name }}</div>
            @endforeach
        </div>
    @endforeach
@endsection